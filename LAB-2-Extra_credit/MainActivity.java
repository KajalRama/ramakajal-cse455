package com.example.android.currencyconverter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private EditText editText01;
   // private EditText editText02;
    private Button bnt01;
    private Button bnt02;
    private TextView textView01;
    private TextView textView02;
    private String usd;
    private String yen;
    private Double result;
   @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editText01 = findViewById(R.id.EditText01);
     //   editText02 = findViewById(R.id.EditText02);
        bnt01 = findViewById(R.id.bnt);
        textView01 = findViewById(R.id.Yen);
        bnt02 = findViewById(R.id.bnt_2);
        textView02 = findViewById(R.id.USD);
        bnt01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                usd = editText01.getText().toString();
                if (usd.equals("")) {
                    textView01.setText("This field cannot be balnk!");
                } else {
                    Double dInputs = Double.parseDouble(usd);
                     result = dInputs * 112.57;
                    textView01.setText("$" + usd + " = " + "Y" + String.format("%.2f", result));
                    editText01.setText("");
                }
            }
        });
        bnt02.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textView01.setText("Y" + String.format("%.2f", result) + " = " +  "$" + usd );
            }
    });
    }
}

