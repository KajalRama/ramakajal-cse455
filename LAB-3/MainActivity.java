package com.example.android.currencyconverter;

import android.os.AsyncTask;
import android.renderscript.Sampler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends AppCompatActivity {
    private EditText editText01;
   // private EditText editText02;
    private Button bnt01;
    private Button bnt02;
    private TextView textView01;
    private TextView textView02;
    private String usd;
    private String yen;
    private Double result;
    private static final String url = "https://api.fixer.io/latest?base=USD";
    String json = "";
    //This string will be assigned each line of the jason string in the loop
    String line = "";
    //variable that will obtain the value we need for currency conversion
    String rate = "";
   @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editText01 = findViewById(R.id.EditText01);
     //   editText02 = findViewById(R.id.EditText02);
        bnt01 = findViewById(R.id.bnt);
        textView01 = findViewById(R.id.Yen);
        bnt02 = findViewById(R.id.bnt_2);
        textView02 = findViewById(R.id.USD);
        bnt01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Double convert = Double.parseDouble(json);
                BackgroundTask object = new BackgroundTask();
                usd = editText01.getText().toString();
                if (usd.equals("")) {
                    textView01.setText("This field cannot be balnk!");
                } else {
                    Double dInputs = Double.parseDouble(usd);
                     result = dInputs * 112.57;
                    textView01.setText("$" + usd + " = " + "Y" + String.format("%.2f", result));
                    editText01.setText("");
                }
            }
        });
        bnt02.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textView01.setText("Y" + String.format("%.2f", result) + " = " +  "$" + usd );
            }
    });
    }
    private class BackgroundTask extends AsyncTask<Void,Void, String>{
       protected void onPreExecute(){super.onPreExecute();}
       protected void onProgressUpdate(Void... values){ super.onProgressUpdate(values);}
       protected void onPostExecute(String result){
           super.onPostExecute(result);
           System.out.println("\n What is rate: " + rate + "\n");
           Double value = Double.parseDouble(rate);
           System.out.println("\nTesting JSON string Exchange Rate INSIDE AsynchTask: " + value);
           usd = editText01.getText().toString();
           if(usd.equals("")){
               textView01.setText("This field can not be empty");
           }
           else{
               Double dInputs = Double.parseDouble(usd);
               Double output = dInputs + value;
               textView01.setText("$" + usd + "-" + "Y"+String.format("%,2f",output));
               editText01.setText("");
           }
       }
       protected String doInBackground(Void... params){
           try {
               URL web_url = new URL(MainActivity.this.url);
               HttpURLConnection httpURLConnection = (HttpURLConnection) web_url.openConnection();
               httpURLConnection.setRequestMethod("GET");
               System.out.println("\nTesting... Before conncetion method to URL\n");
               httpURLConnection.connect();
               InputStream inputStream = httpURLConnection.getInputStream();
               BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
               System.out.println("CONNECTION SUCCESSFUL\n");
               while (line != null) {
                   line = bufferedReader.readLine();
                   json += line;
               }
               System.out.println("\n THE JSON: " + json);
               JSONObject obj = new JSONObject(json);
               JSONObject objRate = obj.getJSONObject("rates");
               rate = objRate.get("JPY").toString();
           } catch(MalformedURLException e) {
               e.printStackTrace();
           } catch (IOException e){
               e.printStackTrace();
           } catch (JSONException e) {
               Log.e("MYAPP","unexpected JSON exception",e);
               System.exit(1);
           }
           return null;
       }
    }
}
